<?php

/**
 * This file is part of the Tracy (https://tracy.nette.org)
 * Copyright (c) 2004 David Grudl (https://davidgrudl.com)
 */

if (!function_exists('dump')) {
	/**
	 * Tracy\Debugger::dump() shortcut.
	 * @tracySkipLocation
	 */
	function dump($var)
	{
	    $a = func_get_args();
		array_map('Tracy_Debugger::dump', $a);
		return $var;
	}
}

if (!function_exists('bdump')) {
	/**
	 * Tracy_Debugger::barDump() shortcut.
	 * @tracySkipLocation
	 */
	function bdump($var)
	{
	    $a = func_get_args();
		call_user_func_array('Tracy_Debugger::barDump', $a);
		return $var;
	}
}
